from django.urls import path

from .views import *

urlpatterns = [
    path('create', Create.as_view()),
    path('edit', Edit.as_view(), name='edit'),
    path('done', Done.as_view(), name='done'),
    path('delete', Delete.as_view(), name='delete'),
    # path('<int:pk>/', DetailTodo.as_view()),
]