from rest_framework import generics
from todoapp.models import Todo,Reminder
from .serializers import TodoSerializer,ReminderSerializer
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse
from django.db.models import Q
from django.db import connection

def success_response(response,message="Success", status_code=None):
    json_obj = {
        "hasError": False,
        "errorCode": -1,
        "message": message,
        "debugMessage":""
    }
    json_obj["response"] = response
    if status_code is None:
        return Response(json_obj, status=status.HTTP_200_OK)
    return Response(json_obj, status=status_code)


# function for returning failure response
def failure_response(response,debugMessage='Invalid', status_code=None, error_code=1001, message="Failure"):
    json_obj = {
        "hasError": True,
        "errorCode": error_code,
        "message": message,
        "debugMessage": debugMessage

    }
    json_obj["response"] = response
    if status_code is None:
        return Response(json_obj, status=status.HTTP_200_OK)
    return Response(json_obj, status_code)

class Create(APIView):

    def post(self, request):
        try:
            title = request.data['title']
            description = request.data['description']
            task = Todo.objects.create()
            task.title = title
            task.description = description
            task.save()
            message = "New Task added successfully"
            response = task.id

            return success_response(response, message)
        except Exception as e:
            response = {}
            return failure_response(response, message="Something wrong with data", debugMessage=str(e))

class Done(APIView):

    def post(self, request):
        try:
            task_id = request.data['taskId']
            reminder_texts = request.data['reminder_text']

            if Todo.objects.filter(Q(id=task_id)).exists():
                for reminder_text in reminder_texts:
                    reminder = Reminder.objects.create()
                    reminder.task_id = task_id
                    reminder.description = reminder_text
                    reminder.save()

                message = "Reminder Added successfully"
                response = ""

                return success_response(response, message)
            else:
                message = "Task Not Found"
                response = {}

                return failure_response(response, message)
        except Exception as e:
            response = {}
            return failure_response(response, message="Something wrong with data", debugMessage=str(e))

class Edit(APIView):

    def post(self, request):
        try:
            taskId = request.data['taskId']
            reminder_texts = request.data['reminder_text']

            if Todo.objects.filter(Q(id=taskId)).exists():
                data=Reminder.objects.filter(task_id=taskId)
                if data:
                    data.delete()
                for reminder_text in reminder_texts:
                    reminder = Reminder.objects.create()
                    reminder.task_id = taskId
                    reminder.description = reminder_text
                    reminder.save()

                message = "Reminder Updated successfully"
                response = ""

                return success_response(response, message)
            else:
                message = "Task not found"
                response = {}

                return failure_response(response, message)
        except Exception as e:
            response = {}
            return failure_response(response, message="Something wrong with data", debugMessage=str(e))

class Delete(APIView):
    def post(self,request):
        try:
            task_id=request.data["taskId"]
            if Todo.objects.filter(id=task_id).exists():
                data = Reminder.objects.filter(task_id=task_id)
                if data:
                    data.delete()
                task_data=Todo.objects.filter(id=task_id)
                if task_data:
                    task_data.delete()
            message="Task removed successfully"
            response= True
            return success_response(response,message)
        except Exception as e:
            response= False
            return failure_response(response,message="Something wrong with data",debugMessage=str(e))




