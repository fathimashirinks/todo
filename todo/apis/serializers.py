from rest_framework import serializers
from todoapp import models


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'title',
            'description',
        )
        model = models.Todo

class ReminderSerializer(serializers.ModelSerializer):
    task_id = TodoSerializer()
    class Meta:
        fields = (
            'id',
            'task_id',
            'description',
            'created_at',
            'updated_at'
        )
        model = models.Reminder